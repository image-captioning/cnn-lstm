# Thêm thư viện
import numpy as np
import matplotlib.pyplot as plt
from pickle import dump, load
from keras.preprocessing.sequence import pad_sequences
from keras.models import load_model

images = 'Flickr8k_Dataset/Flicker8k_Dataset'

with open("data_preprocess/encoded_test_images.pkl", "rb") as encoded_pickle:
    encoding_test = load(encoded_pickle)

# print(type(encoding_test))
# print(encoding_test['/2594902417_f65d8866a8.jpg'])

# open a file, where you stored the pickled data
file_ixtoword = open('data_preprocess/ixtoword.pkl', 'rb')
file_wordtoix = open('data_preprocess/wordtoix.pkl', 'rb')
# dump information to that file
ixtoword = load(file_ixtoword)
wordtoix = load(file_wordtoix)

# open a file, where you stored the pickled data
file_parameter = open('data_preprocess/parameter.pkl', 'rb')
parameter = load(file_parameter)
file_parameter.close()
print(parameter)
max_length = 32
print(parameter)
model = load_model('image_captioning_model.h5')

def greedySearch(photo):
    in_text = 'startseq'
    for i in range(max_length):
        sequence = [wordtoix[w] for w in in_text.split() if w in wordtoix]
        sequence = pad_sequences([sequence], maxlen=max_length)
        yhat = model.predict([photo,sequence], verbose=0)
        yhat = np.argmax(yhat)
        word = ixtoword[yhat]
        in_text += ' ' + word
        if word == 'endseq':
            break
    final = in_text.split()
    final = final[1:-1]
    final = ' '.join(final)
    return final


z=12
pic = list(encoding_test.keys())[z]
image = encoding_test[pic].reshape((1,2048))
x = plt.imread(images+pic)
print(greedySearch(image))
plt.imshow(x)
plt.show()
