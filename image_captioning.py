# Thêm thư viện
import numpy as np
from numpy import array
import string
import os
import glob
from pickle import dump, load

from keras.layers import LSTM, Embedding, Dense, Dropout
from keras.layers.merge import add
from keras.applications.inception_v3 import InceptionV3
from keras.preprocessing import image
from keras.models import Model
from keras import Input
from keras.applications.inception_v3 import preprocess_input
from keras.preprocessing.sequence import pad_sequences
from keras.utils import to_categorical


def load_doc(file_name):
    file = open(file_name, 'r')
    text = file.read()
    file.close()
    return text


file_name = 'Flickr8k_text/Flickr8k.token.txt'
doc = load_doc(file_name)


# print(doc[: 300])

# save caption as dict {id_image : ['caption1','caption2',...]}
def load_descriptions(doc):
    mapping = dict()
    for line in doc.split('\n'):
        tokens = line.split()
        if len(line) < 2:
            continue

        # 1000268201_693b08cb0e.jpg#1	A girl going into a wooden building .
        image_id, image_desc = tokens[0], tokens[1:]
        image_id = image_id.split('.')[0]
        image_desc = ' '.join(image_desc)
        if image_id not in mapping:
            mapping[image_id] = list()
        mapping[image_id].append(image_desc)

    return mapping


descriptions = load_descriptions(doc)


# print(descriptions)

# Preprocessing text
def clean_description(descriptions):
    table = str.maketrans('', '', string.punctuation)
    for key, desc_list in descriptions.items():
        for i in range(len(desc_list)):
            desc = desc_list[i]
            # tokenize
            desc = desc.split()
            # convert to lower case
            desc = [word.lower() for word in desc]
            # remove punctuation from each token
            desc = [w.translate(table) for w in desc]
            # remove hanging 's' and 'a'
            desc = [word for word in desc if len(word) > 1]
            # remove tokens with numbers in them
            desc = [word for word in desc if word.isalpha()]
            # store as string
            desc_list[i] = ' '.join(desc)

    return descriptions


descriptions = clean_description(descriptions)


# print(descriptions['1000268201_693b08cb0e'])
def save_descriptions(descriptions, file_name):
    lines = list()
    for key, desc_list in descriptions.items():
        for desc in desc_list:
            lines.append(key + " " + desc)

    data = '\n'.join(lines)
    file = open(file_name, 'w')
    file.write(data)
    file.close()


save_descriptions(descriptions, 'descriptions.txt')


# Lấy id ảnh tương ứng với dữ liệu train, test, dev
def load_set(file_name):
    doc = load_doc(file_name)
    dataset = list()
    for line in doc.split('\n'):
        if len(line) < 1:
            continue
        name_image = line.split('.')[0]
        dataset.append(name_image)

    return dataset


# load name images of train data
train_images_file = 'Flickr8k_text/Flickr_8k.trainImages.txt'
train = load_set(train_images_file)

# folder images
images = 'Flickr8k_Dataset/Flicker8k_Dataset'
img = glob.glob('Flickr8k_Dataset/Flicker8k_Dataset/*.jpg')

# File chứa các id ảnh để train
# train_images_file = 'Flickr8k/Flickr8k_text/Flickr_8k.trainImages.txt'
# Read the train image names in a set
train_images = set(open(train_images_file, 'r').read().strip().split('\n'))

# Create a list of all the training images with their full path names
train_img = []

for i in img:  # img is list of full path names of all images
    if i[len(images) + 1:] in train_images:  # Check if the image belongs to training set
        train_img.append(i)  # Add it to the list of train images

# print(train_img[0])

# file chua cac id anh test
test_images_file = 'Flickr8k_text/Flickr_8k.testImages.txt'
# Read the validation image names in a set# Read the test image names in a set
test_images = set(open(test_images_file, 'r').read().strip().split('\n'))
test_img = []
for i in img:  # img is list of full path names of all images
    if i[len(images) + 1:] in test_images:  # Check if the image belongs to training set
        test_img.append(i)  # Add it to the list of train images


# add startseq, endseq to sentence
# Thêm 'startseq', 'endseq' cho chuỗi
def load_clean_descriptions(filename, dataset):
    # load document
    doc = load_doc(filename)
    descriptions = dict()
    for line in doc.split('\n'):
        # split line by white space
        tokens = line.split()
        # split id from description
        image_id, image_desc = tokens[0], tokens[1:]
        # skip images not in the set
        if image_id in dataset:
            # create list
            if image_id not in descriptions:
                descriptions[image_id] = list()
            # wrap description in tokens
            desc = 'startseq ' + ' '.join(image_desc) + ' endseq'
            # store
            descriptions[image_id].append(desc)
    return descriptions


# descriptions
train_descriptions = load_clean_descriptions('descriptions.txt', train)


# print('Descriptions: train=%d' % len(train_descriptions))

# Load ảnh, resize về khích thước mà Inception v3 yêu cầu.
def preprocess(image_path):
    # Convert all the images to size 299x299 as expected by the inception v3 model
    img = image.load_img(image_path, target_size=(299, 299))
    # Convert PIL image to numpy array of 3-dimensions
    x = image.img_to_array(img)
    # Add one more dimension
    x = np.expand_dims(x, axis=0)
    # preprocess the images using preprocess_input() from inception module
    x = preprocess_input(x)
    return x


# Load the inception v3 model
model = InceptionV3(weights='imagenet')

# model.summary()
# Tạo model mới, bỏ layer cuối từ inception v3
# delete last layer: predictions (Dense)             (None, 1000)         2049000     avg_pool[0][0]
model_new = Model(model.input, model.layers[-2].output)


# model_new.summary()

# phan encode nen dung thu vien tensorflow with batch de toi uu hoa thoi gian
# Image embedding thành vector (2048, )
def encode(image):
    image = preprocess(image)  # preprocess the image
    fea_vec = model_new.predict(image)  # Get the encoding vector for the image
    fea_vec = np.reshape(fea_vec, fea_vec.shape[1])  # reshape from (1, 2048) to (2048, )
    return fea_vec


# [START]THIS PART USE WHEN YOU HAVE NOT PREPROCESSED IMAGE YET

# Gọi hàm encode với các ảnh trong traning set
# start = time()
# encoding_train = {}
# index_train = 0
# for img in train_img:
#     encoding_train[img[len(images)+1:]] = encode(img)
#     index_train+=1
#     print(index_train)
#
# print(len(encoding_train))
# print("Time taken in seconds =", time()-start)


# Lưu image embedding lại
# with open("data_preprocess/encoded_train_images.pkl", "wb") as encoded_pickle:
#     dump(encoding_train, encoded_pickle)

# Encode test image
# start = time()
# encoding_test = {}
# index_test = 0
# for img in test_img:
#     encoding_test[img[len(images):]] = encode(img)
#     index_test+=1
#     print(index_test)


# print("Time taken in seconds =", time()-start)

# Save the bottleneck test features to disk
# with open("data_preprocess/encoded_test_images.pkl", "wb") as encoded_pickle:
#     dump(encoding_test, encoded_pickle)

# [END]THIS PART USE WHEN YOU HAVE NOT PREPROCESSED IMAGE YET

# pickle file save object. load file return dict of object
# load() Decode the given Python file-like stream containing a JSON formatted value into Python object.
train_features = load(open("data_preprocess/encoded_train_images.pkl", "rb"))

# Tạo list các training caption
all_train_captions = []
for key, val in train_descriptions.items():
    for cap in val:
        all_train_captions.append(cap)

len(all_train_captions)

word_count_threshold = 10
word_count = {}
for caption in all_train_captions:
    for w in caption.split(' '):
        word_count[w] = word_count.get(w, 0) + 1

vocab = [w for w in word_count if word_count[w] >= word_count_threshold]
# print(len(vocab))
# print(len(word_count))

# print(vocab)

# index to word and word to index
ixtoword = {}
wordtoix = {}

ix = 1
for w in vocab:
    ixtoword[ix] = w
    wordtoix[w] = ix
    ix += 1

with open("data_preprocess/wordtoix.pkl", "wb") as encoded_pickle:
    dump(wordtoix, encoded_pickle)

with open("data_preprocess/wordtoix.pkl", "wb") as encoded_pickle:
    dump(wordtoix, encoded_pickle)

vocab_size = len(ixtoword) + 1  # +1 for padding
print(vocab_size)


# convert a dictionary of clean descriptions to a list of descriptions
def to_line(descriptions):
    all_desc = list()
    for key in descriptions.keys():
        for d in descriptions[key]:
            all_desc.append(d)
    return all_desc


# max len of descriptions
def max_length(descriptions):
    all_desc = to_line(descriptions)
    return max(len(d.split()) for d in all_desc)


# save max length
max_length = max_length(descriptions)
# print("max lenght: ", max_length)
dict_max_lenght = {'max_length': max_length}
with open("data_preprocess/parameter.pkl", "wb") as encoded_pickle:
    dump(dict_max_lenght, encoded_pickle)


# data generator cho việc train theo từng batch model.fit_generator()
def data_generator(descriptions, photos, wordtoix, max_length, num_photos_per_batch):
    X1, X2, y = list(), list(), list()
    n = 0

    # loop for ever over images
    while 1:
        for key, desc_list in descriptions.items():
            n += 1
            # retrieve the photo feature
            photo = photos[key + '.jpg']
            for desc in desc_list:
                # encode the sequence
                seq = [wordtoix[word] for word in desc.split(' ') if word in wordtoix]
                # split one sequence into multiple X, y pairs
                for i in range(1, len(seq)):
                    # split into input and output pair
                    in_seq, out_seq = seq[:i], seq[i]
                    # pad input sequence
                    in_seq = pad_sequences([in_seq], maxlen=max_length)[0]
                    # encode output sequence
                    out_seq = to_categorical([out_seq], num_classes=vocab_size)[0]
                    # store
                    X1.append(photo)
                    X2.append(in_seq)
                    y.append(out_seq)
            # yield the batch data
            if n == num_photos_per_batch:
                yield [[array(X1), array(X2)], array(y)]
                X1, X2, y = list(), list(), list()
                n = 0


# Load Glove model
glove_dir = 'glove'

embeddings_index = {}  # empty dictionary
f = open(os.path.join(glove_dir, 'glove.6B.200d.txt'), encoding="utf-8")

for line in f:
    values = line.split()
    word = values[0]
    coefs = np.asarray(values[1:], dtype='float32')
    embeddings_index[word] = coefs
f.close()

print('Found %s word vectors.' % len(embeddings_index))

print('embeddings_index[the]: ', len(embeddings_index['the']))

embedding_dim = 200
print(vocab_size)

embedding_matrix = np.zeros((vocab_size, embedding_dim))

# chi lay nhung word ma ta co trong data
for word, i in wordtoix.items():
    embedding_vector = embeddings_index.get(word)
    if embedding_vector is not None:
        embedding_matrix[i] = embedding_vector

print(embedding_matrix.shape)

inputs1 = Input(shape=(2048,))
fe1 = Dropout(0.5)(inputs1)
fe2 = Dense(256, activation='relu')(fe1)
inputs2 = Input(shape=(max_length,))
se1 = Embedding(vocab_size, embedding_dim, mask_zero=True)(inputs2)
se2 = Dropout(0.5)(se1)
se3 = LSTM(256)(se2)
decoder1 = add([fe2, se3])
decoder2 = Dense(256, activation='relu')(decoder1)
outputs = Dense(vocab_size, activation='softmax')(decoder2)
model = Model(inputs=[inputs1, inputs2], outputs=outputs)

model.summary()

model.layers[2].set_weights([embedding_matrix])
model.layers[2].trainable = False

model.compile(loss="categorical_crossentropy", optimizer='adam', metrics=['accuracy'])
# model.optimizer.lr = 0.0001

# data_generator_test = data_generator(train_descriptions, train_features, wordtoix, max_length, number_pics_per_bath)
model.optimizer.lr = 0.0001
epochs = 10
number_pics_per_bath = 6
steps = len(train_descriptions) // number_pics_per_bath

data_gen = data_generator(train_descriptions, train_features, wordtoix, max_length, number_pics_per_bath)
validation_data, validation_label = next(data_gen)

for i in range(len(train_img) // number_pics_per_bath):
    data, labels = next(data_gen)
    model.fit(data, labels, epochs=2,
              batch_size=32,
              verbose=1,
              validation_data=(validation_data, validation_label),
              validation_steps=len(validation_label) // 32)
    print("batch: ", i)

model.save('image_captioning_model.h5')
